import { Component, Input } from '@angular/core';
import {Score} from "../score";
import {TimerComponent} from "../timer/timer.component";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-scores',
  templateUrl: './scores.component.html',
  styleUrls: ['./scores.component.scss'],
  standalone: true,
  imports: [TimerComponent, NgIf],
})
export class ScoresComponent {
  @Input() score?: Score;

  constructor() {}
}
