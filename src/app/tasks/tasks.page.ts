import {Component,  OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {Task1Page} from "../task1/task1.page";
import {Task2Page} from "../task2/task2.page";
import {Task3Page} from "../task3/task3.page";
import {Task4Page} from "../task4/task4.page";
import {TasksCompletedPage} from "../tasks-completed/tasks-completed.page";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
  standalone: true,
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Task1Page,
    Task2Page,
    Task3Page,
    Task4Page,
    TasksCompletedPage,
    RouterLink,
  ],
})
export class TasksPage implements OnInit {
  showTask1: boolean = true;
  showTask2: boolean = false;
  showTask3: boolean = false;
  showTask4: boolean = false;
  showTasksCompleted: Boolean = false;
  startTime: any;
  times: number[] = [];
  potatoes: boolean[] = [];
  schnitzel: number = 0;
  header:string = "Task 1"

  constructor() {}

  ngOnInit() {
    this.startTime = new Date();
  }

  task1Completed(potato: any) {
    this.header = "Task 2"
    this.showTask1 = false;
    this.showTask2 = true;
    const currentTime = new Date();
    this.times.push((currentTime.getTime() - this.startTime.getTime()) / 1000);
    this.startTime = currentTime;
    this.schnitzel += 1;
    this.potatoes.push(potato);
  }

  task2Completed(potato: any) {
    this.header = "Task 3"
    this.showTask2 = false;
    this.showTask3 = true;
    const currentTime = new Date();
    this.times.push((currentTime.getTime() - this.startTime.getTime()) / 1000);
    this.startTime = currentTime;
    this.schnitzel += 1;
    this.potatoes.push(potato);
  }

  task3Completed(potato: any) {
    this.header = "Task 4"
    this.showTask3 = false;
    this.showTask4 = true;
    const currentTime = new Date();
    this.times.push((currentTime.getTime() - this.startTime.getTime()) / 1000);
    this.startTime = currentTime;
    this.schnitzel += 1;
    this.potatoes.push(potato);
  }

  task4Completed(potato: any) {
    this.header = "Tasks Completed"
    this.showTask4 = false;
    this.showTasksCompleted = true;
    const currentTime = new Date();
    this.times.push((currentTime.getTime() - this.startTime.getTime()) / 1000);
    this.startTime = currentTime;
    this.schnitzel += 1;
    this.potatoes.push(potato);
  }
}
