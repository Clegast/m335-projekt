import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  standalone: true,
})
export class TimerComponent implements OnInit {
  constructor() {}
  @Input() Countdown: number = 0;
  @Input() Goesdown: boolean = true;
  @Output() gotPotato = new EventEmitter<boolean>()
  minutes:number = 0;
  seconds:number = 0;

  ngOnInit() {
    if (this.Goesdown){
      this.countdown();
    }
    else {
      this.frezze()
    }
  }

  frezze(){
    this.minutes =(this.Countdown-this.Countdown%60)/60;
    this.seconds =(this.Countdown-60*this.minutes);
  }

  wait(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async countdown()  {
    while (this.Countdown > 0 && this.Goesdown) {
      this.Countdown = this.Countdown - 1;
      await this.wait(1000);
      this.minutes =(this.Countdown-this.Countdown%60)/60;
      this.seconds =(this.Countdown-60*this.minutes);
    }
    this.gotPotato.emit(true)
  }
}
