import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';;
import { Barcode, BarcodeScanner } from '@capacitor-mlkit/barcode-scanning';
import { AlertController } from '@ionic/angular';
import {Haptics} from "@capacitor/haptics";
import {RouterLink} from "@angular/router";
import {TimerComponent} from "../timer/timer.component";
import { addIcons } from 'ionicons';
import { close } from 'ionicons/icons';

@Component({
  selector: 'app-task3',
  templateUrl: './task3.page.html',
  styleUrls: ['./task3.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, RouterLink, TimerComponent],
})
export class Task3Page {
  @Output() taskCompleted = new EventEmitter<boolean>();
  barcodes: Barcode[] = [];
  result?: any;
  showButton: boolean = false;
  potato: boolean = false;

  constructor(private alertController: AlertController) {addIcons({ close });}

  async scan() {
    if ((await BarcodeScanner.scan()).barcodes[0].rawValue == 'M335@ICT-BZ') {
      this.showButton = true;
      await Haptics.vibrate({ duration: 1000 });
    }
  }

  async requestPermissions(): Promise<boolean> {
    const { camera } = await BarcodeScanner.requestPermissions();
    return camera === 'granted' || camera === 'limited';
  }

  gotPotato(event: any) {
    this.potato = event;
  }

  nextButtonClicked() {
    this.taskCompleted.emit(this.potato);
  }
}

