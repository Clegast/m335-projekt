import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {Device} from "@capacitor/device";
import {RouterLink} from "@angular/router";
import { Haptics, ImpactStyle } from '@capacitor/haptics';
import {TimerComponent} from "../timer/timer.component";
import { addIcons } from 'ionicons';
import { close, flash } from 'ionicons/icons';

@Component({
  selector: 'app-task4',
  templateUrl: './task4.page.html',
  styleUrls: ['./task4.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, RouterLink, TimerComponent],
})
export class Task4Page implements OnInit {
  @Output() taskCompleted = new EventEmitter<boolean>();
  showButton: boolean = false;
  potato: boolean = false;

  constructor() {addIcons({ close, flash });}
  IsCharging: boolean | undefined = false;

  async ngOnInit() {
    await this.ChargingStateHasChanged();
  }

  async ChargingStateHasChanged() {
    while (this.IsCharging == false) {
      let temp = await Device.getBatteryInfo();
      this.IsCharging = temp.isCharging;
      await this.wait(1000);
    }
    await Haptics.vibrate({ duration: 1000 });
  }
  wait(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  gotPotato(event: any) {
    this.potato = event;
  }

  nextButtonClicked() {
    this.taskCompleted.emit(this.potato);
  }
}
