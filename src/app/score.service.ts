import { Injectable } from '@angular/core';
import {Score} from "./score";
import { Preferences } from "@capacitor/preferences";

@Injectable({
  providedIn: 'root'
})
export class ScoreService {

  async addScore(name:string, schnizel:number, potatos: number, time: number):Promise<void>{
    let oldScores = await this.getScores()
    oldScores.push({
      name: name,
      schnizel: schnizel,
      potatos: potatos,
      time: time,
      date: new Date().toDateString()
    })
    await Preferences.set(
      {
        key: "scores",
      value: JSON.stringify(oldScores)
      }
    );
  }

  async getScores(){
    const ret = await Preferences.get({key: "scores"});
    if (ret.value){
      const scores: Score[] = JSON.parse(ret.value);
      console.log(scores)

      return scores;
    }
    else {
      return [];
    }
  }

  async wipeScore(){
    await Preferences.clear();
  }

  constructor() { }
}
