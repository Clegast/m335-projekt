import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {IonButton, IonTabButton} from "@ionic/angular/standalone";
import { RouterModule} from "@angular/router";
import { TimerComponent} from "../timer/timer.component";
import { Geolocation } from "@capacitor/geolocation";
import { addIcons } from 'ionicons';
import { close } from 'ionicons/icons';
import {Haptics} from "@capacitor/haptics";


@Component({
  selector: 'app-task1',
  templateUrl: './task1.page.html',
  styleUrls: ['./task1.page.scss'],
  standalone: true,
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    IonTabButton,
    RouterModule,
    TimerComponent,

  ],
})
export class Task1Page implements OnInit {
  @Output() taskCompleted = new EventEmitter<boolean>();
  showButton = false;
  callbackId: string = '';
  distance: number = 100;
  potato: boolean = false;

  constructor() {
    addIcons({ close });
  }
  async ngOnInit() {
    this.watchPosition();
  }

  async watchPosition() {
    this.callbackId = await Geolocation.watchPosition(
      { enableHighAccuracy: true },
      (value) => this.checkPosition(value),
    );
  }

  async checkPosition(position: any) {
    if (position != null) {
      this.calculateDistance(position);
      var checkLongitude =
        position.coords.longitude <= 8.349 &&
        position.coords.longitude >= 8.3488;
      var checkLatitude =
        position.coords.latitude <= 47.0721 &&
        position.coords.latitude >= 47.0719;
      if (checkLongitude && checkLatitude) {
        Geolocation.clearWatch({ id: this.callbackId });
        this.showButton = true;
        await Haptics.vibrate({ duration: 1000 });
      }
    }
  }

  calculateDistance(position: any) {
    var lon1 = (position.coords.longitude * Math.PI) / 180;
    var lon2 = (8.348947 * Math.PI) / 180;
    var lat1 = (position.coords.latitude * Math.PI) / 180;
    var lat2 = (47.071988 * Math.PI) / 180;

    let dlon = lon2 - lon1;
    let dlat = lat2 - lat1;
    let a =
      Math.pow(Math.sin(dlat / 2), 2) +
      Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);

    let c = 2 * Math.asin(Math.sqrt(a));

    let r = 6371;

    // calculate the result
    this.distance = c * r * 1000;
  }

  gotPotato(event: any) {
    this.potato = event;
  }

  nextButtonClicked() {
    this.taskCompleted.emit(this.potato);
  }
}

