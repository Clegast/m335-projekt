import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {Geolocation} from "@capacitor/geolocation";
import {RouterLink} from "@angular/router";
import {TimerComponent} from "../timer/timer.component";
import { addIcons } from 'ionicons';
import { close } from 'ionicons/icons';
import {Haptics} from "@capacitor/haptics";

@Component({
  selector: 'app-task2',
  templateUrl: './task2.page.html',
  styleUrls: ['./task2.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, RouterLink, TimerComponent],
})
export class Task2Page implements OnInit {
  @Output() taskCompleted = new EventEmitter<boolean>()
  showButton = false;
  callbackId: string = '';
  distance: number = 0;
  positionOld: any;
  potato : boolean = false

  constructor() {
    addIcons({ close });
  }

  ngOnInit() {
    this.watchPosition();
  }

  async watchPosition() {
    this.callbackId = await Geolocation.watchPosition(
      { enableHighAccuracy: true },
      (value) => this.calculateDistance(value),
    );
  }

  async calculateDistance(position: any) {
    if (this.positionOld != null && position != null) {
      var lon1 = (position.coords.longitude * Math.PI) / 180;
      var lon2 = (this.positionOld.coords.longitude * Math.PI) / 180;
      var lat1 = (position.coords.latitude * Math.PI) / 180;
      var lat2 = (this.positionOld.coords.latitude * Math.PI) / 180;

      let dlon = lon2 - lon1;
      let dlat = lat2 - lat1;
      let a =
        Math.pow(Math.sin(dlat / 2), 2) +
        Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);

      let c = 2 * Math.asin(Math.sqrt(a));

      let r = 6371;

      // calculate the result
      this.distance += c * r * 1000;

      if (this.distance >= 20) {
        Geolocation.clearWatch({ id: this.callbackId });
        this.showButton = true;
        await Haptics.vibrate({ duration: 1000 });
      }
    }
    this.positionOld = position;
  }

  gotPotato(event:any) {
    this.potato = event
  }

  nextButtonClicked() {
    this.taskCompleted.emit(this.potato)
  }
}
