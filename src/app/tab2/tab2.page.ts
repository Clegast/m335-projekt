import { Component, OnInit } from '@angular/core';
import {IonHeader, IonToolbar, IonTitle, IonContent, IonButton} from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import {Score} from "../score";
import {ScoreService} from "../score.service";
import {ScoresComponent} from "../scores/scores.component";
import {NgForOf} from "@angular/common";


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  standalone: true,
  imports: [
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    ExploreContainerComponent,
    ScoresComponent,
    NgForOf,
    IonButton,
  ],
})
export class Tab2Page implements OnInit {
  scores: Score[] = [];
  chanches: number = 1;

  constructor(private scoreService: ScoreService) {}
  async ngOnInit(): Promise<void> {
    let tempscores = await this.scoreService.getScores()
    while (this.chanches > 0)
    this.chanches = 0
    for(let i=1;i<tempscores.length; i++){
      if(tempscores[i].time<tempscores[i-1].time){
        let temp = tempscores[i-1];
        tempscores[i-1] =tempscores[i];
        tempscores[i]= temp;
        this.chanches ++
      }
    }
    this.scores = tempscores
  }


}
