import {Component, Input, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {NameService} from "../name.service";
import {ScoreService} from "../score.service";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-tasks-completed',
  templateUrl: './tasks-completed.page.html',
  styleUrls: ['./tasks-completed.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, RouterLink],
})
export class TasksCompletedPage implements OnInit {
  @Input() potatoes: boolean[] = [false, false, false, false];
  @Input() schnitzel: number = 4;
  @Input() times: number[] = [0, 0, 0, 0];
  totalTime: number = 0;
  totalPotatoes: number = 0;
  playerName: any;

  constructor(
    private nameService: NameService,
    private scoreService: ScoreService,
  ) {}

  ngOnInit() {
    this.times.forEach((num) => {
      this.totalTime += num;
    });

    this.potatoes.forEach((potato) => {
      if (potato) this.totalPotatoes += 1;
    });

    this.playerName = this.nameService.getName().toString();
    if (this.times.toString() != [0, 0, 0, 0].toString())
      this.scoreService.addScore(
        this.playerName,
        this.schnitzel,
        this.totalPotatoes,
        this.totalTime,
      );
  }
}
