import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonButton, IonAlert } from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { Router } from "@angular/router";
import { AlertButton } from "@ionic/angular";
import { AlertController } from '@ionic/angular';
import { Geolocation } from "@capacitor/geolocation";
import { BarcodeScanner } from "@capacitor-mlkit/barcode-scanning";
import {Task2Page} from "../task2/task2.page";
import {NameService} from "../name.service";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    ExploreContainerComponent,
    IonButton,
    IonAlert,
    Task2Page,
  ],
})
export class Tab1Page {
  constructor(
    private router: Router,
    private alertController: AlertController,
    private nameService: NameService
  ) {}

  public alertButtons: AlertButton[] = [
    {
      text: 'OK',
      handler: async (value) => {
        this.nameService.setName(value.name)
        await this.showAlert();
      },
    },
  ];

  public alertInputs = [
    {
      name: 'name',
      placeholder: 'Name',
    },
  ];

  protected readonly alert = alert;

  async showAlert() {
    const alert = await this.alertController.create({
      header: 'Request Access',
      subHeader: "",
      message: 'To play the game the app nedds access to your location and camera',
      buttons: [
        {
          text: 'Accept',
          handler: async () => {
            await Geolocation.requestPermissions();
            await BarcodeScanner.requestPermissions();
            await this.router.navigate(['/tasks']);
          },
        },
      ],
    });
    await alert.present();
  }
}
